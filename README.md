# About
Collegedata-Scraper is a chrome extension used to scrape the data from colleagedata.com.

# Dependency
FileSaver
https://github.com/eligrey/FileSaver.js
jquery-3.3.1
https://code.jquery.com/jquery-3.3.1.min.js

# Usage
### Installation
* Step 1:
This extension is not public, so you need to toggle your developer mode in Chrome Extension Management Window (chrome://extensions/).
* Step 2:
Then load unpacked extension folder by clicking this button (Load uppacked).

Select chrome-ex folder.

### Scrape the data
Go the collegedata.com. You will see the scrapper button on the top left corner.

 Create your collegedata.com account (**needed**). Then go to the admission tracker page (https://www.collegedata.com/en/prepare-and-apply/admissions-tracker/).
 Select the year and college you wish to scrape.

  After the results are fetched from their server, click Download on the scraper bar.
  
# Raw data?
If it's on, the STATUS of students will be the raw output.
If it's off, the STATUS of students will be mapped by following dictionary.

```
{
    Applied : Denied
    Will-Attend : Accepted
    Wait-Listed : Denied
    Deferred : Denied
}
```
It means, for old data before, if the STATUS is still 'Applied' or 'Wait-Listed' or 'Deferred', we treat it as 'Denied'.
  
# Notice
1. The process will be slow when you scrape data from multiple years. Be sure to open the Chrome inspector to see the progress.
2. Don't use it to do bad things.



