$(document).ready(function () {
    console.log("Scraper is ready!");
    var overlay = $("#collegedate-scraper");
    if (overlay.length == 0) {
        overlay = $("<div id='collegedate-scraper'><button type='button' id='download-btn' style='margin:5px'>Download</button><input type='checkbox' id='raw-check' name='raw-check' value='false' style='margin:5px'><label for='raw-check' style='margin:5px; color: rgb(255,255,255)'>Raw data?</label></div>");
        overlay.css({
            "position": "fixed",
            "width": "220px",
            "height": "40px",
            "z-index": "99999",
            "top": "20px",
            "left": "20px",
            "background-color": "RGB(71, 114, 158)",
            "cursor": "pointer",
            "-webkit-transition": "all 1000ms cubic-bezier(0.57, 0.06, 0.05, 0.95)",
            "-moz-transition": "all 1000ms cubic-bezier(0.57, 0.06, 0.05, 0.95)",
            "-ms-transition": "all 1000ms cubic-bezier(0.57, 0.06, 0.05, 0.95)",
            "-o-transition": "all 1000ms cubic-bezier(0.57, 0.06, 0.05, 0.95)",
            "transition": "all 1000ms cubic-bezier(0.57, 0.06, 0.05, 0.95)"
        });
    }
    $("body").append(overlay);

    $('#raw-check').change(function () {
        cb = $(this);
        cb.val(cb.prop('checked'));
    });

    var button = $("#download-btn");
    button.click(function (event) {
        event.preventDefault();
        var get_raw = $('#raw-check').val();
        if (get_raw === "false") {
            scrapData(false);
        } else {
            scrapData(true);
        }
        button.attr("disabled", true);
    });


    var full_data_dict = {};

    function scrapData(get_raw) {
        
        console.log("Scraping started!");
        console.log("Geting raw html source data! This might take a few minites! Please be patient!");
        $.get(window.location.href, null, function (html) {
            console.log("Parsing started!");
            var elements = $(html);
            var found = $('.dl-table tbody tr', elements);
            if (found.length == 0) {
                alert("The page doesn't have the data you want!")
                console.log("Scraping stopped!");
                $("#download-btn").attr("disabled", false);
                return;
            }
            found.each(function (index) {
                var line_to_write = '';
                var grade_checker = 0;
                var withdrawn_application = false;
                var current_id = '';
                $('td', this).each(function (i, elem) {

                    var cell_text = $(this).text().trim();
                    if (cell_text.includes('HS Type')) { // high-school type
                        var hs_type = cell_text.replace('HS Type', '').trim().replace(/\s{2,}/g, "\n");
                        line_to_write += hs_type + ',';
                    } else if (cell_text.includes('State')) { // Which State? XX means the student is international.
                        var state = cell_text.replace('State', '').trim().replace(/\s{2,}/g, "\n");
                        line_to_write += state + ',';
                    } else if (cell_text.includes('GPA-W')) { // GPA-W
                        var gpa_w = cell_text.replace('GPA-W', '').trim().replace(/\s{2,}/g, "\n");
                        line_to_write += gpa_w + ',';
                        if (gpa_w !== '') {
                            grade_checker++;
                        }
                    } else if (cell_text.includes('GPA')) { // GPA
                        var gpa = cell_text.replace('GPA', '').trim().replace(/\s{2,}/g, "\n");
                        line_to_write += gpa + ',';
                        if (gpa !== '') {
                            grade_checker++;
                        }
                    } else if (cell_text.includes('SAT-M')) { // SAT-M
                        var sat_m = cell_text.replace('SAT-M', '').trim().replace(/\s{2,}/g, "\n");
                        line_to_write += sat_m + ',';
                        if (sat_m !== '') {
                            grade_checker++;
                        }
                    } else if (cell_text.includes('SAT-W')) { // SAT-W
                        var sat_w = cell_text.replace('SAT-W', '').trim().replace(/\s{2,}/g, "\n");
                        line_to_write += sat_w + ',';
                        if (sat_w !== '') {
                            grade_checker++;
                        }
                    } else if (cell_text.includes('SAT-CR')) { // SAT-CR, the admissions after 2021 have new SAT format that includes SAT-CR.
                        // var sat_cr = cell_text.replace('SAT-CR', '').trim().replace(/\s{2,}/g, "\n");
                        // line_to_write += sat_cr + ',';
                        // if (sat_cr !== '') {
                        //     grade_checker++;
                        // }
                    } else if (cell_text.includes('ACT')) { // ACT
                        var act = cell_text.replace('ACT', '').trim().replace(/\s{2,}/g, "\n");
                        line_to_write += act + ',';
                        if (act !== '') {
                            grade_checker++;
                        }
                    } else if (cell_text.includes('Class Rank')) { // class rank
                        var class_rank = cell_text.replace('Class Rank', '').trim().replace(/\s{2,}/g, "\n");
                        line_to_write += class_rank + ',';
                    } else if (cell_text.includes('Status')) { // Target: Accepted or denied
                        var status = cell_text.replace('Status', '').trim().replace(/\s{2,}/g, "\n");
                        if (!get_raw) {
                            if (status === 'Withdrawn') { // the withdrawn application will be removed from dataset
                                withdrawn_application = true;
                            }
                            if (status === 'Applied' || status === 'Wait-Listed' || status === 'Deferred') {
                                status = 'Denied';
                            }
                            if (status === 'Will Attend') {
                                status = 'Accepted';
                            }
                        }
                        line_to_write += status + ',';
                    } else if (cell_text.includes('EA-ED')) { // EA-ED
                        var ea_ed = cell_text.replace('EA-ED', '').trim().replace(/\s{2,}/g, "\n");
                        line_to_write += ea_ed + ',';
                    } else if (cell_text.includes('Legacy')) { // Legacy
                        var legacy = cell_text.replace('Legacy', '').trim().replace(/\s{2,}/g, "\n");
                        line_to_write += legacy + ',';
                    } else if (cell_text.includes('Athlete')) { // Athlete
                        var athlete = cell_text.replace('Athlete', '').trim().replace(/\s{2,}/g, "\n");
                        line_to_write += athlete + ',';
                    } else { // id
                        // check whether has string
                        var id = cell_text.trim();
                        if (id !== '') {
                            line_to_write += id + ',';
                            current_id = id;
                        }
                    }
                });

                if (grade_checker !== 0 && withdrawn_application == false) {
                    full_data_dict[current_id] = line_to_write;
                }
            });
            var allll_count = 0;
            for(var key in full_data_dict) {
                allll_count += 1;
            }

            var promise_count = 0;
            for(var key in full_data_dict) {
                var profile_link = '../admissions-tracker-profile/' + key;
                scrapProfileData(key, profile_link).then(function(result) {
                    var str = full_data_dict[result['id']];
                    str += result['sat_cr'] + ',';
                    str += result['gender'] + ',';
                    str += result['ethnicity'];
                    full_data_dict[result['id']] = str;
                    console.log(`${str}`);
                }).finally(function() {
                    promise_count += 1;
                    console.log(`${promise_count} + ${allll_count}`);
                    if (promise_count === allll_count) {
                        console.log("finished");
                        save_to_file(full_data_dict);
                        full_data_dict = {};
                    }
                });
            }
            $("#download-btn").attr("disabled", false);
        }, "text").fail(function () {
            alert("Scraping failed.");
            $("#download-btn").attr("disabled", false);
        });
    }

    function isNumeric(num){
        return !isNaN(num)
    }

    function save_to_file(dict) {
        var string_to_write = 'id,hs_type,state,GPA,GPA-W,SAT-M,SAT-W,ACT,class_rank,status,EA-ED,legacy,athlete,SAT-CR,gender,ethnicity\n';
        for(var key in dict) {
            string_to_write += dict[key] + '\n';
        }

        var blob = new Blob([string_to_write], { // Creates the file
            type: "text/plain;charset=utf-8"
        });
        saveAs(blob, "data.csv"); // Supposedly save's it (this is the problem)
        console.log("Scraping finished!");
    }

    function scrapProfileData(id, profile_link) {
        return new Promise(function(resolve, reject) {
            var detail_data_dict = {
                'id': id,
                'gender': '',
                'ethnicity': '',
                'sat_cr': ''
            };
            $.get(profile_link, function (html) {
                console.log('Parsing Profile for id: ' + id);
                var elements = $(html);
                var personal_data = $('#main > div:nth-child(2) > div > div.profile-header > div.profile-header__details.media > div > dl', elements);
                personal_data.each(function (index, elem) {
                    var pd = $(elem);
                    var found_gender = false;
                    var found_ethnicity = false;
                    pd.children().each(function (index, elem) {
                        var cell_text = $(this).text().trim();
                        if (cell_text.includes('Gender')) {
                            found_gender = true;
                        } else if (cell_text.includes('Ethnicity')) {
                            found_ethnicity = true
                        } else {
                            if (found_ethnicity == true) {
                                detail_data_dict['ethnicity'] = cell_text;
                                found_ethnicity = false;
                            }
                            if (found_gender == true) {
                                detail_data_dict['gender'] = cell_text;
                                found_gender = false;
                            }
                        }
                    });
                });

                var tableRow = elements.find("td").filter(function() {
                    return $(this).text() == "Critical Reading";
                }).closest("tr");
                tableRow.children().each(function (index, elem) {
                    var text = $(this).text().trim();
                    if (isNumeric(text)) {
                        detail_data_dict['sat_cr'] = text;
                    }
                });
                resolve(detail_data_dict);
            }).fail(function() {
                reject();
            });
        });
    }
});

